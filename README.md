# AutoCorrect_For_Zettlr

## Introduction

A little tool to modify the file “config.json” for the software ZETTLR. The aim is to set with efficiency the autocorrection setting.

## License

GNU GPL v3


## Project status

First version to alpha test.
